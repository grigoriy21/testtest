<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 9/25/2017
 * Time: 11:55 AM
 */

namespace app\models;


use yii\db\ActiveRecord;

class Product extends ActiveRecord
{
    public static function tableName()
    {
        return 'products';
    }
    public function  getCategories(){
        return $this->hasOne(Category::className(), ['id' => 'parent']);
    }
}
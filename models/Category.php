<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 9/25/2017
 * Time: 11:55 AM
 */

namespace app\models;


use yii\db\ActiveRecord;

class Category extends ActiveRecord
{
    public static function tableName()
    {
        return 'categories';
    }

    public function  getProducts(){
        return $this->hasMany(Product::className(), ['parent' => 'id']);
    }
}
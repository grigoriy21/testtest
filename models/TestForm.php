<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 9/24/2017
 * Time: 3:39 PM
 */

namespace app\models;
use  yii\db\ActiveRecord;

class TestForm extends ActiveRecord {

    public static function tableName()
    {
        return 'posts';
    }

    public  function attributeLabels()
    {
        return [
            'name' => 'Имя',
            'email' => 'E-mail',
            'text' => 'Текст сообщения',
        ];
    }

    public function rules()
    {
        return [
            [['name','text'], 'required'],
            ['email', 'trim'],
        ];
    }

}
<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 9/22/2017
 * Time: 2:32 PM
 */

namespace app\controllers;

use yii\web\Controller;

class AppController extends Controller
{
    public function debug($arr){
        echo '<pre>'.print_r($arr, true).'</pre>';
    }
}


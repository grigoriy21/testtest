<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 9/22/2017
 * Time: 5:29 AM
 */

namespace app\controllers;



class MyController extends AppController {

    public function actionIndex($id = null){
        $hi = 'Hello, world!';
        $names = ['Ivanov', 'Petrov', 'sidorov'];

        if(!$id) $id = 'test';
//        return $this->render('index', ['hello' => $hi, 'names' => $names]);
        return $this->render('index', compact('hi', 'names', 'id'));
    }

    public function  actionBlogPost(){
        return 'Blog Post';
    }
}